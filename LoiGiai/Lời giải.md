# Lời giải - thông tin thêm

ExploraScience, 3/2019

## Bài thơ của Ibn al-Bannâ

Ký hiệu $x$ là số phần trái tim tác giả được chia ra, thì chúng ta có phương trình sau:
$$
\frac{3}{7}+\frac{1}{7}+\frac{1}{7}+\frac{1}{2}\times\frac{1}{7}+\frac{1}{4}\times\frac{1}{7}+\frac{1}{7}+\frac{1}{6}\times\frac{1}{4}\times\frac{1}{7}+\frac{5}{x}=1
$$
Giải phương trình này chúng ta thu được $x=168$.

## 4 người đi mua ngựa

Gọi $x, y, z, t$ lần lượt là số tiền người thứ nhất, người thứ hai, người thứ ba và người thứ tư mang theo. N là giá tiền của con ngựa thồ. Chúng ta có hệ phương trình:
$$
\begin{cases}
x+\frac{1}{2}y+\frac{1}{2}z+\frac{1}{2}t = N \hspace{20pt}(1)\\
\frac{1}{3}x+y+\frac{1}{3}z+\frac{1}{3}t = N \hspace{20pt}(2)\\
\frac{1}{4}x+\frac{1}{4}y+z+\frac{1}{4}t = N \hspace{20pt}(3)\\
\frac{1}{5}x+\frac{1}{5}y+\frac{1}{5}z+t = N \hspace{20pt}(4)\\
\end{cases}
$$
Hệ 4 phương trình này có 5 ẩn, nên để có thể giải chúng ta sẽ phải quy ước giá trị của 1 ẩn, ví dụ quy ước giá tiền con ngựa bằng 1 số bất kỳ, $N=37$ đồng chẳng hạn. Việc này là chấp nhận được vì trong trường hợp này giá trị tuyệt đối của con ngựa là bao nhiêu không quan trọng; điều quan trọng là 4 người đàn ông mang theo bao nhiêu tiền so với giá tiền của con ngựa.

Trước hết chúng ta có thể dùng các phép thay thế để thể hiện $x, y, z$ theo $t$ và $N$. Ví dụ:
$$
2\times(1) - 5\times(4) = -3N \\
2x+y+z+t - (x+y+z+5t) = -3N \\
x-4t=-3N \\
x=4t-3N
$$
Tương tự:
$$
2\times(1) - 3\times(2) = -N \\
2x+y+z+t - (x+3y+z+t) = -N \\
x-2y=-N \\
y=\frac{1}{2}x+\frac{1}{2}N \\
y=\frac{1}{2}(4t-3N)+\frac{1}{2}N \\
y=2t-N
$$
Và:
$$
2\times(1) - 4\times(3) = -2N \\
2x+y+z+t - (x+y+4z+t) = -2N \\
x-3z=-2N \\
z=\frac{1}{3}x+\frac{2}{3}N \\
z=\frac{1}{3}(4t-3N)+\frac{2}{3}N \\
z=\frac{4}{3}t-\frac{1}{3}N
$$
Thay các biểu thức tìm được của $x, y, z​$ theo $t​$ vào $(1)​$, chúng ta có:
$$
(4t-3N)+\frac{1}{2}(2t-N)+\frac{1}{2}(\frac{4}{3}t-\frac{1}{3}N)+\frac{1}{2}t = N \\
\frac{37}{6}t - \frac{22}{6}N = N \\
t = \frac{28}{37}N
$$
Với $N=37​$, chúng ta có $x=1, y=19, z=25, t=28​$.

## Đếm số lượng đôi thỏ sau 1 năm

Chúng ta thử làm 1 cách thủ công:

| Đầu Tháng thứ | Số đôi thỏ con | Số đôi trưởng thành | Số Đôi thỏ CON sẽ sinh ra trong tháng |
| ------------- | -------------- | ------------------- | ------------------------------------- |
| 1             | 1              | 0                   | 0                                     |
| 2             | 0              | 1                   | 1                                     |
| 3             | 1              | 1                   | 1                                     |
| 4             | 1              | 2                   | 2                                     |
| 5             | 2              | 3                   | 3                                     |
| 6             | 3              | 5                   | 5                                     |
| 7             | 5              | 8                   | 8                                     |
| 8             | 8              | 13                  | 13                                    |
| 9             | 13             | 21                  | 21                                    |
| 10            | 21             | 34                  | 34                                    |
| 11            | 34             | 55                  | 55                                    |
| 12            | 55             | 89                  | 89                                    |

Vậy sau 12 tháng sẽ có tổng cộng $55+89+89=233$ đôi thỏ.

Dãy số 1, 1, 2, 3, 5, 8... được gọi là chuỗi Fibonacci. Chuỗi này có nhiều đặc điểm thú vị. Ví dụ phân số tạo thành bởi các số hạng liền nhau trong chuỗi: 34/21, 55/34, 89/55... sẽ tiến tới *tỉ số vàng*, rất được ưa chuộng trong mỹ thuật.

## Những người đàn ông và những chiếc bánh mỳ

Gọi người đàn ông thứ nhất là A, người thứ hai là B. Mỗi người ăn 5/3 ổ bánh.

Vì A mang 3 ổ, nên có thể coi người lính đã "mua" của A 3 - 5/3 = 4/3 ổ. Tương tự, người lính đã "mua" của B 2 - 5/3 = 1/3 ổ.

Như vậy A đã cho người lính lượng bánh gấp 4 lần lượng bánh B cho người lính. Do đó A phải được chia 4 đồng, còn B 1 đồng.

## Ông vua trồng cây

**Giải theo cách tiểu học, tức "thuần túy số học"**

30 người trồng trong 9 ngày được 1000 cây, như vậy  
1 người trồng trong 1 ngày được $\frac{1000}{30\times9}$ cây, và  
36 người trồng trong 1 ngày được $\frac{36\times1000}{30\times9}$ cây.

Do đó, số ngày để 36 người trồng được 4400 cây là $\frac{4400}{\frac{36\times1000}{30\times9}} = 33$ ngày.

**Giải theo cách đại số**

Số lượng cây trồng được T tỉ lệ thuận với số ngày D và số người M, do đó:
$$
T = k\times D\times M \\
1000 = k\times9\times30 \\
k = \frac{1000}{9\times30}
$$
Chúng ta cần tính D biết $T=4000$ và $M=36$:
$$
D = \frac{T}{k\times M} = \frac{4000}{\frac{1000}{9\times30}\times 36} = 33
$$

## Thùng phuy có 4 lỗ thoát

**Giải theo "phương pháp số học"**

Với lỗ thoát thứ nhất, 1 ngày thùng phuy sẽ tháo hết nước được 1 lần.  

Với lỗ thoát thứ hai, 1 ngày thùng phuy sẽ tháo hết được 1/2 lượng nước.  

Với lỗ thoát thứ ba, 1 ngày thùng phuy sẽ tháo hết được 1/3 lượng nước.  

Với lỗ thoát thứ tư, 1 ngày thùng phuy sẽ tháo hết được 1/4 lượng nước.

Vậy với cả 4 lỗ thoát được mở, 1 ngày thùng phuy sẽ tháo hết được 1+1/2+1/3+1/4=25/12 lượng nước.

Tức là cần 12/25 = 0,48 ngày hay 11,52 giờ để thùng phuy tháo hết nước khi mở cả 4 lỗ thoát.

**Đại số**

Tốc độ rút nước của thùng thứ nhất là 1/24 thùng/h,  
của thùng thứ hai là 1/48 thùng/h,  
của thùng thứ ba là 1/72 thùng/h,  
của thùng thứ tư là 1/96 thùng/h.

Vậy tốc độ rút nước khi mở cả 4 lỗ thoát là 1/24 + 1/48 + 1/72 + 1/96 = 25/288 thùng/h.

Vậy cần 288/25 = 11,52h để thùng xả hết nước khi mở cả 4 lỗ thoát.

## Thuyền sang sông

Ký hiệu các ông chồng là C1, C2, C3, các bà vợ là V1, V2, V3. Bờ xuất phát là B1, bờ cần đến là B2.

| Lượt | B1 trước đi            | Đi     | B1 sau đi      | Về     | b2 sau về      |
| ---- | ---------------------- | ------ | -------------- | ------ | -------------- |
| 1    | C1, C2, C3, V1, V2, V3 | C1, V1 | C2, C3, V2, V3 | C1     | V1             |
| 2    | C1, C2, C3, V2, V3     | V2, V3 | C1, C2, C3     | V1     | V2, V3         |
| 3    | C1, C2, C3, V1         | C2, C3 | C1, V1         | C2, V2 | C3, V3         |
| 4    | C1, C2, V1, V2         | C1, C2 | V1, V2         | V3     | C1, C2, C3     |
| 5    | V1, V2, V3             | V1, V2 | V3             | C3     | C1, C2, V1, V2 |
| 6    | C3, V3                 | C3, V3 |                |        |                |

## Send more money

9567 + 1085 = 10652

## Cầu Koenigsberg

Con đường Euler chỉ tồn tại nếu đồ thị đó có tối đa 2 đỉnh có số bậc lẻ.

Chu trình Euler chỉ tồn tại nếu tất cả các đỉnh của đồ thị đều có bậc chẵn.

## Đường đi của quân Mã

![](assets/quân mã.png)

## Ô vuông ma thuật

(*nguồn: http://villemin.gerard.free.fr/Wwwgvmm/CarreMag*)

Lưu ý: trong hình dưới 2 ô 3x3 và 4x4 xuất phát từ 0. Trong ô 5x5, lưu ý các số 1, 2, 3... được điền theo cách nhảy của con mã: sang phải 1 bước, lên trên 2 bước (và lộn vòng nếu ra ngoài biên)

![](assets/3 hình vuông ma thuật.png)

**Phân tích 3x3**

Ở ô vuông 3x3, tổng mỗi hàng có giá trị (1+2+3+...+9)/3 = 15.

Số 15, khi phân tách thành tổng của 3 số hạng, thì vừa đúng có 8 cách:
$$
1+5+9=15 \\
1+6+8=15 \\
2+4+9=15 \\
2+5+8=15 \\
2+6+7=15 \\
3+4+8=15 \\
3+5+7=15 \\
4+5+6=15 \\
$$
Chúng ta thấy:  
Số 5 xuất hiện 4 lần  
Số 2, 4, 6, 8 xuất hiện 3 lần  
Số 1, 3, 5, 7 xuất hiện 2 lần.

Như vậy, số 5 phải nằm ở giữa; số 2, 4, 6, 8 nằm ở góc; số 1, 3, 7, 9 nằm ở cạnh.

![](assets/vị trí số 5.png)

Chỉ có số 15 mới có tính chất đặc biệt, trùng hợp với câu đố như vậy.

**Cách điền theo đường chéo**

(*nguồn: http://villemin.gerard.free.fr/Wwwgvmm/CarreMag*)

![](assets/điền theo đường chéo 3x3.png)

![](assets/điền theo đường chéo 5x5.png)

Lưu ý: lộn vòng nếu ra ngoài biên

**Tổng quát**

(*nguồn: http://villemin.gerard.free.fr/Wwwgvmm/CarreMag*)

![](assets/3x3 tổng quát.png)

2 ô vuông la-tinh 3x3 được xếp sao cho khi ghép vào, không có cặp số nào trùng nhau.

Thay vì điền các số từ 1 đến 3, cần tạm điền từ 0 đến 2. Gọi ô 3x3 bên trái là Q, ô 3x3 bên phải là R.

Ô vuông ma thuật sẽ là M = 3Q+R.