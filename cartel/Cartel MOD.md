# Những cartel hướng dẫn chơi

## Kim tự tháp

>Trong hình học, kim tự tháp là những hình khối có đáy là 1 đa giác, và mặt bên là các tam giác, tụ lại 1 đỉnh.

Hãy ghép các khối bi để tạo thành các kim tự tháp có đáy nằm lọt trong khuôn cho sẵn !

>Vượt ngoài khuôn khổ Toán học, việc chiến thắng *sức ỳ tâm lý* sẽ đóng vai trò quan trọng trong việc tìm ra lời giải.

## Con lắc hỗn độn

Biết được chuyển động và vị trí hiện tại của con lắc cùng các lực tác động là có thể tính toán được chuyển động tiếp theo của nó, vì vậy chuyển động của nó là tất định. Nhưng liệu sự tất định có luôn đi cùng với việc có thể đoán trước ?

Hãy thử đặt con lắc cùng một vị trí và thả tay. Chuyển động của 2 lần có giống nhau không ?

>Các hệ hỗn loạn bắt đầu được nghiên cứu từ cuối thế kỷ XIX với những khám phá đầy bất ngờ. Các hệ này dù đơn giản nhưng rất khó đoán trước do một sai lệch nhỏ của điều kiện ban đầu dẫn đến những kết quả rất khác nhau. Lý thuyết hỗn loạn có nhiều áp dụng trong khí tượng học, xã hội học, kinh tế, sinh học, tin học... và cả triết học.

## Định lý Py-ta-go

>Định lý Py-ta-go mang tên nhà triết học Hy lạp cổ đại Py-ta-go, nhưng dường như đã được nhiều nền văn hóa chỉ ra trước đó. Bạn có thể chứng minh định lý đảo của Py-ta-go không ?

Bạn có tin vào định lý Py-ta-go: a^2+b^2=c^2 ? Hãy thử thao tác trên các thí nghiệm vật lý và lắp ghép hình học của chúng tôi nhé.

>Có một "định lý Py-ta-go tổng quát" cho các tam giác bất kỳ, được phát biểu bởi Pappus ở Alexandria từ thế kỷ thứ IV.

## Các hằng đẳng thức

Nếu có ai trót quên các *hằng đẳng thức đáng nhớ* liên quan đến (a+b)^2 và (a+b)^3 thì thử tìm lại bằng mô hình này nhé.

## Conway's cube

> Khối ghép lập phương đơn giản này có thể coi là khối đơn giản nhất trong các khối ghép mà John Conway nghiên cứu.

Không khó khăn gì để ghép 9 miếng gỗ đơn giản này để tạo thành khối lập phương 3x3x3 phải không nào ? Nhưng sau khi ghép xong, các bạn thử nghĩ xem liệu có cách phân tích đặc điểm hình học nào cho phép có được lời giải mà không phải thử nhiều lần không ?

## Soma cube

Nếu trò chơi Conway’s cube 3x3 quá dễ dàng với bạn, thì hãy thử sức với Soma cube.

>Trò chơi này được sáng tạo bởi nhà thơ và nhà toán học người Đan Mạch, Piet Hein năm 1933, nhưng được John Conway nghiên cứu kỹ càng trong cuốn *Các cách chắc chắn để chiến thắng các trò chơi Toán học* xuất bản năm 1982.

Bạn sẽ phải dùng đến một thuật toán rất thông dụng trong tin học, đó là *thử sai quay lui*. Hãy đảm bảo bạn không bỏ lỡ tình huống dẫn đến lời giải trong quá trình tìm kiếm của mình nhé. Hãy tìm được ít nhất 1 trong 240 lời giải nhé !

## Thuyền sang sông

>Có rất nhiều câu đố mang chủ đề *Thuyền sang sông*. Phiên bản này xuất hiện trong cuốn "Những vấn đề lý thú và khoái trá giải quyết bằng những con số" năm 1612 của Bachet de Méziriac.

3 người đàn ông ghen tuông cùng 3 bà vợ cần sang sông, nhưng họ chỉ thấy một chiếc thuyền không có người lái, nhỏ đến mức chỉ chứa được 2 người một lúc. Làm thế nào để cả 6 người sang được sông, mà không để xảy ra tình huống 1 bà vợ ở một mình với 1 hay 2 người đàn ông không phải chồng mình ?

## Cây cầu Königsberg

Trong các đồ thị sau, đồ thị nào miêu tả bài toán Cây cầu Königsberg ?

![](./assets/konigsberg - do thi.png)

(*nguồn ảnh: http://discrete.openmathbooks.org/dmoi2/sec_paths.html*)

Bạn hãy thử tìm cách vẽ những hình trên mà không nhấc bút xem (sau đó hãy thử vẽ, không nhấc bút và quay lại điểm ban đầu). Hình nào các bạn vẽ được, hình nào không vẽ được ?

Một vài định nghĩa:

> - Một **con đường Euler** là một đường đi trên đồ thị mà đi qua tất cả các cạnh đúng 1 lần
> - Một **chu trình Euler** là một đường đi trên đồ thị mà đi qua tất cả các cạnh đúng 1 lần và quay trở lại đỉnh xuất phát.
> - **Số bậc** của một đỉnh là số lượng cạnh nối với đỉnh đó.

Gợi ý:

* Có sự liên quan gì đến số bậc của các đỉnh không ?
* Thử vẽ một đồ thị trong đó số bậc của tất cả các đỉnh đều là 2. Có tồn tại con đường Euler và chu trình Euler nào trong đồ thị đó không ?

## Đường đi của quân Mã

Hãy chọn ô xuất phát của quân Mã và đặt số 1 vào trong đó. Với mỗi bước nhảy tiếp theo, bạn hãy đặt lần lượt số 2, số 3... Liệu bạn có thể đặt kín hình vuông bằng các số từ 1 đến 25 không ?

> Quân Mã trong cờ Vua đi theo đường chéo của một hình chữ nhật 3x2.

## Các ô vuông ma thuật

- Đặt các số từ 1 đến 9 vào ô vuông 3x3 sao cho tổng các hàng, cột và đường chéo lớn bằng nhau
- Đặt các số từ 1 đến 16 vào ô vuông 4x4 sao cho tổng các hàng, cột và đường chéo lớn bằng nhau
- Đặt các số từ 1 đến 25 vào ô vuông 5x5 sao cho tổng các hàng, cột và đường chéo lớn bằng nhau

> Euler đã nghiên cứu kỹ càng các hình vuông như thế này. Họ hàng của các ô vuông ma thuật là các ô vuông la-tinh, trong đó ô vuông nxn được điền các con số từ 1 đến n (được lặp lại) sao cho mỗi hàng, mỗi cột đều có các số từ 1 đến n. Lời giải tổng quát cho các ô vuông ma thuật dựa trên việc kết hợp 2 ô vuông la-tinh.

Với ô vuông 3x3, hãy thử giải bằng cách phân tích vị trí đặt các con số thay vì mò mẫm hoàn toàn nhé.

Gợi ý: Số nào sẽ phải nằm ở giữa ?

## 15 miếng ghép

Để giải quyết (hay không giải quyết) câu đố đắt giá này của Sam Loyd, các bạn thử phân tích xem cần số bước như thế nào để ô 14 và 15 đổi chỗ cho nhau, và cần số bước như thế nào để ô trống quay trở lại vị trí ban đầu ?